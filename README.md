# tkpdf

Pdf viewer widget for tkinter. Beta version.

```
Copyright (C) 2020 Roshan Paswan - convert pdf to image data
Copyright (C) 2021 rdbende - tkpdf
```


Here is a not really useful screenshot:
![image](https://user-images.githubusercontent.com/77941087/129190350-54ece240-2df5-4ece-86c9-b041428724f4.png)
